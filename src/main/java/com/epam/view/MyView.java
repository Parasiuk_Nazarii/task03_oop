package com.epam.view;

import com.epam.controller.*;
import com.epam.model.Appliance;

import java.sql.SQLOutput;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - add new appliance");
        menu.put("2", "  2 - delete appliance by name");
        menu.put("3", "  3 - show all appliances");
        menu.put("4", "  4 - show all turned-on appliances");
        menu.put("5", "  5 - show all appliances sorted by power");
        menu.put("6", "  6 - show all appliances in the specified ranges ");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() {
        Appliance appliance = new Appliance();
        String name;
        int power;
        boolean isOn = false;
        do {
            System.out.println("Input Appliance name");
            name = input.next();
            if (controller.isNameUsed(name)) {
                System.out.println("The name is used before. Try other;");
            }
        } while (controller.isNameUsed(name));
        System.out.println("Input it power");
        power = input.nextInt();
        System.out.println("Press '1' if it's online at this moment");
        String pressed = input.next();
        if (pressed.equals("1")) {
            isOn = true;
        }
        appliance.setName(name);
        appliance.setPower(power);
        appliance.setOn(isOn);
        controller.add(appliance);
    }

    private void pressButton2() {
        System.out.println("Please input appliance name");
        String name = input.next();
        if (controller.delete(name)) {
            System.out.println("Success deleted");
        } else System.out.println("Can not find");
    }

    private void pressButton3() {
        controller.getAllAppliances().forEach(Appliance::print);
    }

    private void pressButton4() {
        controller.turnedOnAppliances().forEach(Appliance::print);
        int sum = controller.turnedOnAppliances()
                .stream()
                .mapToInt(Appliance::getPower)
                .sum();
        System.out.println("Total consumption " + sum + "W");
    }

    private void pressButton5() {
        controller.sortedByPowerAppliances().forEach(Appliance::print);

    }

    private void pressButton6() {
        boolean correct = false;
        int bottom;
        int top;
        do {
            System.out.println("Input bottom limit");
            bottom = input.nextInt();
            System.out.println("Input top limit");
            top = input.nextInt();
            if (top > bottom) {
                correct = true;
            } else System.out.println("Input please correct range!");
        } while (!correct);
        controller.appliancesFromRange(bottom, top)
                .forEach(Appliance::print);

    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
