package com.epam.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Domain {
    private List<Appliance> applianceList = new ArrayList<Appliance>();
    private static Scanner input = new Scanner(System.in);

    public List<Appliance> getAllAppliances() {
        return applianceList;
    }

    public void setApplianceList(List<Appliance> applianceList) {
        this.applianceList = applianceList;
    }

    public boolean isNameUsed(String name) {
        boolean used = false;
        for (Appliance app : applianceList) {
            if (app.getName().equals(name)) {
                used = true;
                break;
            }
        }
        return used;
    }

    public void add(Appliance appliance) {
        applianceList.add(appliance);
    }

    public boolean delete(String name) {
        boolean success = false;
        for (Appliance app : applianceList) {
            if (app.getName().equals(name)) {
                applianceList.remove(app);
                success = true;
                break;
            }

        }
        return success;
    }

    public void print() {
        applianceList.forEach(a -> a.print());
    }

    public List<Appliance> turnedOnAppliances() {
        List<Appliance> applianceListOn = applianceList.
                stream().filter(a -> a.isOn()).
                collect(Collectors.toList());
        return applianceListOn;
    }

    public List<Appliance> sortedByPowerAppliances() {
        List<Appliance> sortedList = applianceList
                .stream().sorted(Comparator.comparing(Appliance::getPower))
                .collect(Collectors.toList());
        return sortedList;
    }

    public List<Appliance> appliancesFromRange(int bottom, int top) {

        List<Appliance> applianceRangeList = new ArrayList<>();
        for (Appliance a : applianceList) {
            if ((a.getPower() >= bottom) && (a.getPower() <= top)) {
                applianceRangeList.add(a);
            }
        }
        return applianceRangeList;
    }
}
