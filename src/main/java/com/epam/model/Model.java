package com.epam.model;

import java.util.List;

public interface Model {

    List<Appliance> getAllAppliances();

    void add(Appliance appliance);

    boolean isNameUsed(String name);

    boolean delete(String name);

    void print();

    List<Appliance> turnedOnAppliances();

    List<Appliance> sortedByPowerAppliances();

    List<Appliance> appliancesFromRange(int bottom, int top);
}
