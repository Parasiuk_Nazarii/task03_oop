package com.epam.model;

import com.epam.view.Printable;

public class Appliance implements Printable {
    private String name;
    private int power;
    private boolean isOn;

    public Appliance(String name, int power, boolean isOn) {
        this.name = name;
        this.power = power;
        this.isOn = isOn;
    }

    public Appliance() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean on) {
        isOn = on;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Appliance)) return false;

        Appliance appliance = (Appliance) o;

        return getName().equals(appliance.getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public void print() {
        String onPrint = name + " " + power + " " + ((isOn) ? "On" : "Off");
        System.out.println(onPrint);
    }
}
